// Operator Overloading

#include<iostream>

class Demo{

	public:
		int x=10;
		void getData(){
			std::cout<<"In getData"<<std::endl;
		}
};

int main(){

	Demo obj;

	std::cout<<obj<<std::endl;//no match for ‘operator<<’
	
	// ostream& operator<<(const ostream& cout,const Demo obj)
	return 0;
}

