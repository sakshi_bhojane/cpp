// Operator Overloading  Using Freind function
/*
 * Use perfect match for funcytion otherwise error occured...
 */


#include<iostream>

class Demo{

	int x=10;
	int y=20;

	public:
		Demo(int x,int y){
			this->x = x;
			this->y=y;
		}

		friend int operator+(const Demo obj1,const Demo obj2);
		friend int operator+(const Demo& obj1,const int a){
			std::cout<<"There...."<<std::endl;
			return obj1.x + a;
		}
};

int operator+(const Demo obj1,const Demo obj2){
	
	std::cout<<"Here...."<<std::endl;
	return obj1.x + obj2.y;

}

int main(){

	Demo obj1(30,40);
	Demo obj2(40,40);

	std::cout<<obj1+obj2<<std::endl;//70
	std::cout<<obj1+70<<std::endl;// 100
	std::cout<<80+obj2<<std::endl;// Error

	return 0;

}

