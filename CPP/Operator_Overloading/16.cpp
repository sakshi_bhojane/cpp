 // Operator Overloading
 

#include<iostream>

class Demo{

	int x=10;

	public: 
		Demo(int x){
			//std::cout<<"In constructor"<<std::endl;
			this -> x=x;
		}

		friend int operator/(const Demo& obj1,const Demo& obj2){
			return obj1.x / obj2.x;
		}
};


int main(){

	Demo obj1(50);
	

	std::cout<<obj1/20<<std::endl; //2

	return 0;

}



