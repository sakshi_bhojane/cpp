 // Operator Overloading
 

#include<iostream>

class Demo{

	int x=10;

	public: 
		Demo(int x){
			//std::cout<<"In constructor"<<std::endl;
			this -> x=x;
		}

		friend int operator/(const Demo& obj1,const Demo& obj2){
			return obj1.x / obj2.x;
		}
};


int main(){

	Demo obj1(20);
	

	std::cout<<100/obj1<<std::endl; // 5

	return 0;

}



