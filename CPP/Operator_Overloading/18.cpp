 // Operator Overloading
 

#include<iostream>

class Demo{

	int x=10;

	public: 
		Demo(int x){
			this -> x=x;
		}

		int operator/(const Demo& obj2){
			 std::cout<<"Member.. "<<std::endl;
			 return this->x/obj2.x;
		}
};


int main(){

	Demo obj1(20);
	

	std::cout<<100/obj1<<std::endl; // error: no match for ‘operator/’ (operand types are ‘int’ and ‘Demo’)

	return 0;

}



