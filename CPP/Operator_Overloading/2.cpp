// Operator Overloading

#include<iostream>

class Demo{

	public:
		int x=10;
		void getData(){
			std::cout<<"In getData"<<std::endl;
		}
		friend std::ostream& operator<<(std::ostream& cout,Demo& obj);
};

std::ostream& operator<<(std::ostream& cout,Demo& obj){
	
	std::cout<<obj.x; //	10
	return cout;
}

int main(){

	Demo obj;

	std::cout<<obj<<std::endl;
	
	// ostream& operator<<(const ostream& cout,const Demo obj)
	return 0;
}

