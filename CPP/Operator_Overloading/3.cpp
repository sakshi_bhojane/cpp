// Operator Overloading

#include<iostream>

class Demo{

	public:
		int x=10;
		void getData(){
			std::cout<<"In getData"<<std::endl;
		}
		friend int operator+(Demo& obj1,Demo& obj2);
};

int operator+(Demo& obj1,Demo& obj2){
	
	return obj1.x + obj2.x;

}

int main(){

	Demo obj1;
	Demo obj2;

	std::cout<<obj1+obj2<<std::endl;//20
	
	// int operator+(Demo& obj1, Demo& obj2)
	return 0;
}

