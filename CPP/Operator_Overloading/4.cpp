// Operator Overloading

#include<iostream>

class Demo{

	int x=10;
	Demo(){
	
	}
};

int main(){
	
	int x=10;

	std::cout<<x<<std::endl;
	
	// operator<<(cout,x);                => function call
	// prototype; 
	// ostream& operator<<(ostream&,int);   => predefined
	
	Demo obj;
	std::cout<<obj<<std::endl;
	
	// operator<<(cout,obj);                => function call
	// prototype; 
	// ostream& operator<<(ostream&,Demo);   => not predefined
	
	return 0;
}

