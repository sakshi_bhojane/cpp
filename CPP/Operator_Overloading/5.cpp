// Operator Overloading

#include<iostream>

class Demo{
	
	int x=10;

	public:
		Demo(int x){
			this->x=x;
		}

		
};

int main(){

	Demo obj1(30);
	Demo obj2(40);

	std::cout<<obj1+obj2<<std::endl;// Error no match for opearator+
	
	// int operator+(Demo& obj1, Demo& obj2)
	
	return 0;
}

