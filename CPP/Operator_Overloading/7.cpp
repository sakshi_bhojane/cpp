// Operator Overloading  Using Freind function

#include<iostream>

class Demo{

	int x=10;

	public:
		Demo(int x){
			this->x = x;
		}

		friend int operator+(const Demo& obj1,const Demo& obj2);
};

int operator+(const Demo& obj1,const Demo& obj2){
	
	std::cout<<"Here...."<<std::endl;
	return obj1.x + obj2.x;

}

int main(){

	Demo obj1(30);
	Demo obj2(40);

	std::cout<<obj1+obj2<<std::endl;//70
	std::cout<<obj1+70<<std::endl;  //100
	
	// int operator+(Demo& obj1, Demo& obj2)

	return 0;

}

