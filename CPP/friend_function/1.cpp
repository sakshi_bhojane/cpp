// Friend function

#include<iostream>

class Demo{

	int x=10; // private

	protected:
	int y=20;//protected
	public:

		Demo(){
			std::cout<<"Constructor"<<std::endl;
		}	

		void getdata(){
			std::cout<<x<<std::endl;  
			std::cout<<y<<std::endl;   
		}
};

int main(){
		
	Demo obj;
	std::cout<<obj.x<<std::endl;// x is private within this context
	std::cout<<obj.y<<std::endl; //y is protected within this context

	return 0;
}

