// Friend function

#include<iostream>

class Demo{

	int x=10; // private

	protected:
	int y=20;//protected
	public:

		Demo(){
			std::cout<<"Constructor"<<std::endl;
		}	

		void getdata(){
			std::cout<<x<<std::endl;  
			std::cout<<y<<std::endl;   
			
		}

		friend void accessData(Demo& obj);// parameter is always reference of class
};

void accessData(Demo& obj){// reference call ...avoid use of copy constructor

			std::cout<<obj.x<<std::endl;  
			std::cout<<obj.y<<std::endl; 
}

int main(){
		
	Demo obj;

	accessData(obj);

	return 0;
}
/* Output
  Constructor
  10
  20
*/



