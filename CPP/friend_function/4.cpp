// Friend function

#include<iostream>

class Demo{

	int x=10; // private

	protected:
	int y=20;//protected
	public:

		Demo(){
			std::cout<<"Constructor"<<std::endl;
		}	

		void getdata(){
			std::cout<<"x = "<<x<<std::endl;  
			std::cout<<"y = "<<y<<std::endl;   
			
		}

		friend void accessData(Demo& obj);// parameter is always reference of class
};

void accessData(Demo& obj){// reference call ...avoid use of copy constructor

			int temp=obj.x;
			obj.x=obj.y;
			obj.y=temp;
}

int main(){
		
	Demo obj;

	obj.getdata();
	accessData(obj);
	obj.getdata();

	return 0;
}
/* Output
 *
Constructor
x = 10
y = 20
x = 20
y = 10

*/



