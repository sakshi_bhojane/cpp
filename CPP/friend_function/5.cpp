// Friend function

#include<iostream>

class Demo{

	int x=10; // private

	protected:
	int y=20;//protected
	public:

		Demo(){
			std::cout<<"Constructor"<<std::endl;
		}	

		void getdata(){
			std::cout<<"x = "<<x<<std::endl;  
			std::cout<<"y = "<<y<<std::endl;   
			
		}

		friend void accessData(const Demo& obj);// parameter is always reference of class
};

void accessData(const Demo& obj){// reference call ...avoid use of copy constructor
			
			std::cout<<obj.x<<std::endl; 
			std::cout<<obj.y<<std::endl;  

}

int main(){
		
	Demo obj;

	obj.getdata();
	accessData(obj);
	obj.getdata();

	return 0;
}
/* Output
 *
Constructor
x = 10
y = 20
10
20

*/



