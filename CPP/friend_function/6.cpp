// Mutual Friend (One function is friend function of multiple class

#include<iostream>

class Two;  // forward declaration
class One{

	int x=10; 

	protected:
	
	int y=20;
	
	public:

		One(){
			std::cout<<" One Constructor"<<std::endl;
		}
	private:	

		void getdata()const{// because const object only access const thing
			std::cout<<"x = "<<x<<std::endl;  
			std::cout<<"y = "<<y<<std::endl;   
			
		}

		friend void accessData(const One& obj1, const Two& obj2);
};

class Two{

	int a=10; 

	protected:
	
	int b=20;
	
	public:

		Two(){
			std::cout<<" Two Constructor"<<std::endl;
		}
	private:	

		void getdata()const{
			std::cout<<"a = "<<a<<std::endl;  
			std::cout<<"b = "<<b<<std::endl;   
			
		}

		friend void accessData(const One& obj1, const Two& obj2);
};

void accessData(const One& obj1,const Two& obj2){
			
			obj1.getdata();
			obj2.getdata();

}

int main(){
		
	One obj1;
	Two obj2;

	accessData(obj1,obj2);


	return 0;
}
/* Output
  One Constructor
 Two Constructor
x = 10
y = 20
a = 10
b = 20

*/



