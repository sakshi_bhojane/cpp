// Friend class
 

#include<iostream>

class Two;

class One{
	
	int x=10;
	protected:
	int y=20;

	public:
		One(){
			std::cout<<"One constructor"<<std::endl;
		}
		
		friend class Two;
};

class Two{

	public:
		Two(){
			std::cout<<"Two constructor"<<std::endl;
		}

	private:
		void getData(const One& obj1){
			std::cout<<obj1.x<<std::endl;
			std::cout<<obj1.y<<std::endl;
		}
	public:
		void accessData(const One& obj1){	
		
			std::cout<<obj1.x<<std::endl;
			std::cout<<obj1.y<<std::endl;
		}
};

int main(){
	
	One obj1;
	Two obj2;

	obj2.accessData(obj1);

	return 0;
}
/*
 * OUTPUT
 One constructor
Two constructor
10
20

*/
